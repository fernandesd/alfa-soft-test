<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title inertia>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="/public/css/app.css">

        <!-- Scripts -->
        @routes
        <script src="/public/js/app.js" defer></script>
        @inertiaHead
    </head>
    <body class="font-sans antialiased">
        @inertia

        @env ('local')
            <script src="http://localhost:3000/browser-sync/browser-sync-client.js"></script>
        @endenv
    </body>
    <footer>
        <p class="p-5 text-center">
             <a href="https://gitlab.com/fernandesd/alfa-soft-test" target="_blank" rel="noopener noreferrer"><i class="fa-brands fa-gitlab"></i> Repository on GitLab</a>
        </p>
 </footer>
</html>
