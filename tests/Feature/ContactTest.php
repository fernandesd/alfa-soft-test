<?php

namespace Tests\Feature;

use App\Models\Contact;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Pagination\Paginator;
use Inertia\Testing\AssertableInertia as Assert;
use Tests\TestCase;


class ContactTest extends TestCase
{
    use RefreshDatabase;

    public function testShoudGetListOfContacts()
    {
        Contact::factory(30)->create();

        $response = $this->get(route('contact.index'));
        $response->assertOk();
        $response->assertInertia(
            fn (Assert $page) => $page
            ->component('Contact/Index')
            ->has('contacts')
            ->has('contacts.data')
            ->has('contacts.links')
            ->count('contacts.data', 15) //15 per page
            ->where('contacts.total', 30) //total records
            ->etc()
        );
    }
}
