<?php

namespace App\Http\Controllers;

use App\Actions\Fortify\UpdateUserPassword;
use App\Http\Requests\Contact\StoreRequest;
use App\Http\Requests\Contact\UpdateRequest;
use App\Models\Contact;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index()
    {
        $searchText = request('searchText');
        if(request('searchText')){
            $contacts =  Contact::where('name', 'like', "%{$searchText}%")->orWhere('email', 'like', "%{$searchText}%")->paginate(15);
        }else{
            $contacts = Contact::paginate(15);
        }

        return Inertia::render('Contact/Index',compact('contacts', 'searchText'));
    }

    public function show(Contact $contact)
    {
        return Inertia::render('Contact/Show', compact('contact'));
    }

    public function create(){
        return Inertia::render('Contact/Create');
    }

    public function edit(Contact $contact)
    {
        return Inertia::render('Contact/Edit', compact('contact'));
    }

    public function store(StoreRequest $request)
    {
        Contact::create($request->all());
        return redirect()->route('contacts.index');
    }

    public function update(UpdateRequest $request, Contact $contact)
    {
        $contact->update($request->all());
        return redirect()->route('contacts.index');
    }

    public function destroy(Contact $contact)
    {
        $contact->delete();
        return redirect()->route('contacts.index');
    }
}
